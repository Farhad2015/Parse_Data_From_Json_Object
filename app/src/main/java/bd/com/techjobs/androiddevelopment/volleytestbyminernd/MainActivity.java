package bd.com.techjobs.androiddevelopment.volleytestbyminernd;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    JSONArray array;
    JSONObject object;

    ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = new ArrayList<>();

        JsonObjectRequest objRequest = new JsonObjectRequest(Request.Method.GET, "http://api.androidhive.info/volley/person_object.json", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String a = response.get("name").toString();
                    String b = response.get("email").toString();
                    JSONObject c = response.getJSONObject("phone");
                    String First = c.get("home").toString();
                    String Second = c.get("mobile").toString();
                    Toast.makeText(getApplicationContext(), a+ "//"+b+"//"+First+"Second"+Second, Toast.LENGTH_SHORT).show();
                    Log.e("Second", Second);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        ServiceHelper.getInstance().addToRequest(objRequest);
    }
}
